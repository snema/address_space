Add new fb for beremiz
========================
- correct file beremiz\plcopen\Additional_Function_Blocks.xml
    add pou like this (with no matter body filling if will use "c" code),
    all name with upper case 
```xml
...
      <pou name="WRITE_DO" pouType="functionBlock">
        <interface>
          <inputVars>
            <variable name="DO_VALUE">
              <type>
                <UINT/>
              </type>
              <initialValue>
                <simpleValue value="0"/>
              </initialValue>
              <documentation>
                <xhtml:p><![CDATA[writing do]]></xhtml:p>
              </documentation>
            </variable>
            <variable name="DO_MASK">
              <type>
                <UINT/>
              </type>
              <initialValue>
                <simpleValue value="0"/>
              </initialValue>
              <documentation>
                <xhtml:p><![CDATA[writing do mask]]></xhtml:p>
              </documentation>
            </variable>
          </inputVars>
        </interface>
        <body>
          <ST>
            <xhtml:p><![CDATA[DO_VALUE := 0;]]></xhtml:p>
          </ST>
        </body>
      </pou>
...
```
- correct file \matiec\lib\sofi.txt (this file included in standard_FB.txt)
   add new fb on st (no matter body filling if will use "c")
```
...
    (****************************************************************

                 write do with mask

    ****************************************************************)

    FUNCTION_BLOCK WRITE_DO
      VAR_INPUT
        DO_VALUE : UINT := 0;
        DO_MASK : UINT := 0;
      END_VAR

      DO_VALUE := 0;
    END_FUNCTION_BLOCK
...
```
- correct file \sofi\generator\template\freertos\sofi_beremiz.c
    add two function like this 
```cpp
...
void WRITE_DO_init__(WRITE_DO *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->DO_VALUE,0,retain)
  __INIT_VAR(data__->DO_MASK,0,retain)
}

// Code part
void WRITE_DO_body__(WRITE_DO *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_name[] = "do_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
      regs_access_t reg;
      reg.value.op_u16 = (u16)__GET_VAR(data__->DO_VALUE,) | (((u16)__GET_VAR(data__->DO_MASK,))<<4);
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if(p_link_functions->regs_set((u16)address,reg)==0){
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
      }else{
          __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
      }
  }
  return;
} // WRITE_DO_body__()
...
```
- correct file \sofi\generator\template\freertos\sofi_beremiz.h
    add struct and function description 
```cpp
...
// FUNCTION_BLOCK WRITE_DO
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DO_VALUE)
  __DECLARE_VAR(UINT,DO_MASK)
  // FB private variables - TEMP, private and located variables
} WRITE_DO;
void WRITE_DO_init__(WRITE_DO *data__, BOOL retain);
void WRITE_DO_body__(WRITE_DO *data__);
...
```

