﻿How to change can address space
=============================
* First add parameters to regs.h with "**&can**", "**&can + &pdot**" or "**&can + &pdor**" file like:

```cpp
          u16 ai_unit[AI_NUMS];           //!<"14 bit capacity or 12 bit capacity for analog inputs",&ro &can &pdot
          
          u32 run_test;                   //!<"running tests",&ro &can
```
"&can" - for sdo read write parameters, only when master will send command;

"&can + pdot" for pdo , will transmit data every sync command;

"&can + pdor" for pdo , it's will receive data every sync command;

For compitible with old configuration of beremiz , new var with &can options should be placed at the end.

* Compile programm ("cmake --build . --target all") and check changes in files tools/generators/"&name_module+file.xml"(up to you module for exmpl it's  bricfile.xml or aifile.xml) , .

* Replace file in path C:\Beremiz\beremiz\module\ 


