SOFI(DI) Address space
========================
|index|name|type|size|byte_address|mdb_address|flags|description|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|0|mdb_addr|U16_REGS_FLAG|1|0|0|SELF, SAVED|  "modbus address",  |
|1|device_type|U8_REGS_FLAG|1|2|1|SELF, READ_ONLY, SAVED|  "type of device",     |
|2|board_ver|U8_REGS_FLAG|1|3|1|SELF, READ_ONLY, SAVED|  "board version",     |
|3|module_number|U16_REGS_FLAG|1|4|2|SELF, SAVED|  "module ao number 0 - 127",    |
|4|num_of_vars|U16_REGS_FLAG|1|6|3|SELF|   "number of vars self + config(user)   |
|5|ip|U8_REGS_FLAG|4|8|4|SELF, SAVED|  "ip address",  |
|6|netmask|U8_REGS_FLAG|4|12|6|SELF, SAVED|  "netmask address",  |
|7|gateaway|U8_REGS_FLAG|4|16|8|SELF, SAVED|  "gateaway address",  |
|8|usb_local_ip|U8_REGS_FLAG|4|20|10|SELF, SAVED|  "ip address for local usb net",   |
|9|mdb_revers|U8_REGS_FLAG|1|24|12|SELF, SAVED|  "reverse 3 and 4 function", |
|10|mdb_shift|U8_REGS_FLAG|1|25|12|SELF, SAVED|  "shift start address regs from 0 to 1", |
|11|reset_num|U16_REGS_FLAG|1|26|13|SELF, READ_ONLY, SAVED|  "number of system resets",  |
|12|last_reset|U16_REGS_FLAG|1|28|14|SELF, READ_ONLY, SAVED|  "reason of last system reset",     See rst_reason_t |
|13|sys_tick_counter|U64_REGS_FLAG|1|30|15|SELF, READ_ONLY|   "tick in ms",  |
|14|tick100us|U64_REGS_FLAG|1|38|19|SELF, READ_ONLY|  "tick counter in 100us time", |
|15|time_hms|U8_REGS_FLAG|10|46|23|SELF|  "struct for real time" |
|16|unix_time_sec|S32_REGS_FLAG|1|56|28|SELF|  "since the Epoch (00:00:00 UTC, January 1, 1970)"  |
|17|os_version|U8_REGS_FLAG|4|60|30|SELF, READ_ONLY|  "version by 0.1.1",  |
|18|mac_addr|U8_REGS_FLAG|6|64|32|SELF, READ_ONLY|  "mac address",  |
|19|uniq_id|U8_REGS_FLAG|12|70|35|SELF, READ_ONLY|  "uniq_id number" ,  |
|20|internal_temp|FLOAT_REGS_FLAG|1|82|41|SELF, READ_ONLY|  "temperature internal sense value",  |
|21|v_pwr|FLOAT_REGS_FLAG|1|86|43|SELF, READ_ONLY|   "PWR voltage"   |
|22|v_bat|FLOAT_REGS_FLAG|1|90|45|SELF, READ_ONLY|   "3V battery voltage"   |
|23|cur_free_heap|U32_REGS_FLAG|1|94|47|SELF, READ_ONLY|   in bytes, |
|24|min_free_heap|U32_REGS_FLAG|1|98|49|SELF, READ_ONLY|   in bytes, |
|25|di_test_result|U32_REGS_FLAG|1|102|51|SELF, READ_ONLY|  "di test result",   DI[0...15] = {1-OK, 0-error}, StateError[16], CounterError[17], FrequencyError[18]; |
|26|sofi_test_result|U32_REGS_FLAG|1|106|53|SELF, READ_ONLY|  "sofi_test blocks results",  Results[0...31] = {see sofi_test_block_t, 1-OK, 0-failed}; |
|27|sofi_test_blocks|U32_REGS_FLAG|1|110|55|SELF|  "sofi test blocks"    Blocks[0...28] = {see sofi_test_block_t},CheckTest[29],StressTest[30],PerfTest[31]; |
|28|run_test|U32_REGS_FLAG|1|114|57|SELF, READ_ONLY|  "running tests",  |
|29|state|U32_REGS_FLAG|1|118|59|SELF, READ_ONLY|   "current module state"   |
|30|command|U16_REGS_FLAG|1|122|61|SELF|   "command register"   See @ref sofi_command_t  |
|31|debug_info|U8_REGS_FLAG|8|124|62|SELF|  "reserved use for debug" |
|32|uart1_sets|U16_REGS_FLAG|1|132|66|SELF, SAVED|  "settings immodule uart",  BitRateValue[0...3] = {0-default,1-2400,2-4800,3-9600,4-14400, |
|33|uart3_sets|U16_REGS_FLAG|1|134|67|SELF, SAVED|  "settings DEBUG_UART",  WordLen[4,5] = {0-7bit,1-8bit,2-9bit}; |
|34|channels_timeout|U32_REGS_FLAG|6|136|68|SELF, SAVED|   "time outs for channel use for retranslations", |
|35|di_noise_fltr_us|U16_REGS_FLAG|16|160|80|SELF, SAVED|  "digital inputs noise filter in us (x10)",  |
|36|di_pulseless_ms|U32_REGS_FLAG|16|192|96|SELF, SAVED|  "digital inputs pulseless time in ms",  |
|37|di_mode|U16_REGS_FLAG|16|256|128|SELF, SAVED|  "digital inputs mode",   see di_mode_t  |
|38|di_state|U32_REGS_FLAG|1|288|144|SELF, READ_ONLY, SAVED|  "digital inputs state",   |
|39|di_cnt|U64_REGS_FLAG|16|292|146|SELF, SAVED|  "digital inputs cnt values",  |
|40|di_freq|FLOAT_REGS_FLAG|16|420|210|SELF, READ_ONLY, SAVED|  "digital inputs frequency values",  |
|41|flags_task|U32_REGS_FLAG|1|484|242|SELF, READ_ONLY|  "check for task created"  |
|42|counter_task|U64_REGS_FLAG|4|488|244|SELF, READ_ONLY|  "struct counter tasks"  |
|43|flags_init_passed|U32_REGS_FLAG|1|520|260|SELF, READ_ONLY|  "inited modules"  for init in task |
|44|flags_succ_init|U32_REGS_FLAG|1|524|262|SELF, READ_ONLY|  "success inited modules"  for init in task |
|45|isol_pwr_state|U16_REGS_FLAG|1|528|264|SELF, READ_ONLY|  "isolated power state",  |
|46|ai_internal|U16_REGS_FLAG|4|530|265|SELF, READ_ONLY|  "adc internal service channels", |
|47|rs_485_immo_sends|U32_REGS_FLAG|1|538|269|SELF|   "RS-485_1 send num" |
|48|rs_485_immo_errors|U32_REGS_FLAG|1|542|271|SELF|   "RS-485_1 errors" |
|49|monitor_period|U32_REGS_FLAG|1|546|273|SELF, READ_ONLY|   "sofi_monitor period in ms", |
|50|total_tasks_time|FLOAT_REGS_FLAG|1|550|275|SELF, READ_ONLY|   "sum of running times of tasks in %", |
|51|task|U8_REGS_FLAG|28|554|277|SELF, READ_ONLY|  tasks information, |
|52|task|U8_REGS_FLAG|28|582|291|SELF, READ_ONLY|  tasks information, |
|53|task|U8_REGS_FLAG|28|610|305|SELF, READ_ONLY|  tasks information, |
|54|task|U8_REGS_FLAG|28|638|319|SELF, READ_ONLY|  tasks information, |
|55|task|U8_REGS_FLAG|28|666|333|SELF, READ_ONLY|  tasks information, |
|56|task|U8_REGS_FLAG|28|694|347|SELF, READ_ONLY|  tasks information, |
|57|task|U8_REGS_FLAG|28|722|361|SELF, READ_ONLY|  tasks information, |
|58|task|U8_REGS_FLAG|28|750|375|SELF, READ_ONLY|  tasks information, |
|59|task|U8_REGS_FLAG|28|778|389|SELF, READ_ONLY|  tasks information, |
|60|task|U8_REGS_FLAG|28|806|403|SELF, READ_ONLY|  tasks information, |
|61|task|U8_REGS_FLAG|28|834|417|SELF, READ_ONLY|  tasks information, |
|62|task|U8_REGS_FLAG|28|862|431|SELF, READ_ONLY|  tasks information, |
|63|task|U8_REGS_FLAG|28|890|445|SELF, READ_ONLY|  tasks information, |
|64|task|U8_REGS_FLAG|28|918|459|SELF, READ_ONLY|  tasks information, |
|65|task|U8_REGS_FLAG|28|946|473|SELF, READ_ONLY|  tasks information, |
|66|task|U8_REGS_FLAG|28|974|487|SELF, READ_ONLY|  tasks information, |
|67|task|U8_REGS_FLAG|28|1002|501|SELF, READ_ONLY|  tasks information, |
|68|task|U8_REGS_FLAG|28|1030|515|SELF, READ_ONLY|  tasks information, |
|69|task|U8_REGS_FLAG|28|1058|529|SELF, READ_ONLY|  tasks information, |
|70|task|U8_REGS_FLAG|28|1086|543|SELF, READ_ONLY|  tasks information, |
|71|task|U8_REGS_FLAG|28|1114|557|SELF, READ_ONLY|  tasks information, |
|72|task|U8_REGS_FLAG|28|1142|571|SELF, READ_ONLY|  tasks information, |
|73|task|U8_REGS_FLAG|28|1170|585|SELF, READ_ONLY|  tasks information, |
|74|task|U8_REGS_FLAG|28|1198|599|SELF, READ_ONLY|  tasks information, |
|75|task|U8_REGS_FLAG|28|1226|613|SELF, READ_ONLY|  tasks information, |
|76|task|U8_REGS_FLAG|28|1254|627|SELF, READ_ONLY|  tasks information, |
|77|task|U8_REGS_FLAG|28|1282|641|SELF, READ_ONLY|  tasks information, |
|78|task|U8_REGS_FLAG|28|1310|655|SELF, READ_ONLY|  tasks information, |
|79|task|U8_REGS_FLAG|28|1338|669|SELF, READ_ONLY|  tasks information, |
|80|task|U8_REGS_FLAG|28|1366|683|SELF, READ_ONLY|  tasks information, |
|81|task|U8_REGS_FLAG|28|1394|697|SELF, READ_ONLY|  tasks information, |
|82|task|U8_REGS_FLAG|28|1422|711|SELF, READ_ONLY|  tasks information, |
|83|link|U16_REGS_FLAG|1|1450|725|SELF, READ_ONLY|    |
|84|eth_arp|U16_REGS_FLAG|1|1452|726|SELF, READ_ONLY|    |
|85|ip_frag|U16_REGS_FLAG|1|1454|727|SELF, READ_ONLY|    |
|86|ip_proto|U16_REGS_FLAG|1|1456|728|SELF, READ_ONLY|    |
|87|icmp|U16_REGS_FLAG|1|1458|729|SELF, READ_ONLY|    |
|88|udp|U16_REGS_FLAG|1|1460|730|SELF, READ_ONLY|    |
|89|tcp|U16_REGS_FLAG|1|1462|731|SELF, READ_ONLY|    |
|90|mem_heap|U16_REGS_FLAG|1|1464|732|SELF, READ_ONLY|   used heap memory by lwip, |
|91|memp_udp_pool|U16_REGS_FLAG|1|1466|733|SELF, READ_ONLY|    |
|92|memp_tcp_pool|U16_REGS_FLAG|1|1468|734|SELF, READ_ONLY|    |
|93|memp_listen_tcp|U16_REGS_FLAG|1|1470|735|SELF, READ_ONLY|    |
|94|memp_seg_tcp|U16_REGS_FLAG|1|1472|736|SELF, READ_ONLY|    |
|95|memp_altcp|U16_REGS_FLAG|1|1474|737|SELF, READ_ONLY|    |
|96|memp_reassdata|U16_REGS_FLAG|1|1476|738|SELF, READ_ONLY|    |
|97|memp_frag_pbuf|U16_REGS_FLAG|1|1478|739|SELF, READ_ONLY|    |
|98|memp_net_buf|U16_REGS_FLAG|1|1480|740|SELF, READ_ONLY|    |
|99|memp_net_conn|U16_REGS_FLAG|1|1482|741|SELF, READ_ONLY|    |
|100|memp_tcpip_api|U16_REGS_FLAG|1|1484|742|SELF, READ_ONLY|    |
|101|memp_tcpip_input|U16_REGS_FLAG|1|1486|743|SELF, READ_ONLY|    |
|102|memp_sys_timeout|U16_REGS_FLAG|1|1488|744|SELF, READ_ONLY|    |
|103|memp_pbuf_ref|U16_REGS_FLAG|1|1490|745|SELF, READ_ONLY|    |
|104|memp_pbuf_pool|U16_REGS_FLAG|1|1492|746|SELF, READ_ONLY|    |
|105|lwip_sys|U16_REGS_FLAG|1|1494|747|SELF, READ_ONLY|    |
