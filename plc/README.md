SOFI(PLC) Address space
========================
|index|name|type|size|byte_address|mdb_address|flags|description|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|0|mdb_addr|U16_REGS_FLAG|1|0|0|SELF, SAVED|  "modbus address",  |
|1|mdb_revers|U8_REGS_FLAG|1|2|1|SELF, SAVED|  "reverse 3 and 4 function", |
|2|mdb_shift|U8_REGS_FLAG|1|3|1|SELF, SAVED|  "shift start address regs from 0 to 1", |
|3|ip|U8_REGS_FLAG|4|4|2|SELF, SAVED|  "ip address", |
|4|netmask|U8_REGS_FLAG|4|8|4|SELF, SAVED|  "netmask address", |
|5|gateaway|U8_REGS_FLAG|4|12|6|SELF, SAVED|  "gateaway address", |
|6|eth_speed|U8_REGS_FLAG|1|16|8|SELF, SAVED|  "speed10-100mb",          {0 - auto 10 - 10 100 - 100} |
|7|eth_duplex|U8_REGS_FLAG|1|17|8|SELF, SAVED|  "duplex full or half",     {0 - auto 1 - HALF 2 - FULL} |
|8|reset_num|U16_REGS_FLAG|1|18|9|SELF, READ_ONLY, SAVED|  "number of system resets", |
|9|last_reset|U16_REGS_FLAG|1|20|10|SELF, READ_ONLY, SAVED|  "reason of last system reset",     See rst_reason_t |
|10|user_task_state|U16_REGS_FLAG|1|22|11|SELF, READ_ONLY, SAVED|  "user task current state", |
|11|user_task_config|U16_REGS_FLAG|1|24|12|SELF, SAVED|  "user task config", |
|12|uart1_sets|U16_REGS_FLAG|1|26|13|SELF, SAVED|  "settings MESO_UART",     BitRateValue[0...3] = {0-default,1-2400,2-4800,3-9600,4-14400, |
|13|uart2_sets|U16_REGS_FLAG|1|28|14|SELF, SAVED|  "settings RS_485_2",      5-19200,6-28800,7-38400,8-56000,9-57600,10-76800,11-115200,12-1200}; |
|14|uart3_sets|U16_REGS_FLAG|1|30|15|SELF, SAVED|  "settings RS_232",        WordLen[4,5] = {0-7bit,1-8bit,2-9bit}; |
|15|uart5_sets|U16_REGS_FLAG|1|32|16|SELF, SAVED|  "settings RS_485_1",      StopBit[6,7] = {1-1bit,2-2bit}; |
|16|uart6_sets|U16_REGS_FLAG|1|34|17|SELF, SAVED|  "settings RS_485_IMMO",   Parity[8,9] = {0-none,1-odd,2-even}; |
|17|uart7_sets|U16_REGS_FLAG|1|36|18|SELF, SAVED|  "settings HART",          RxDelay[10...15] {Delay(sec) = RxDelay * 20 / BitRateValue}; |
|18|channels_timeout|U32_REGS_FLAG|7|38|19|SELF, SAVED|   "time outs for channel use for retranslations", |
|19|do_state|U8_REGS_FLAG|1|66|33|SELF, READ_ONLY|  "state of digital output",   DO_ON[0...3], DO_SC[4...7]; |
|20|do_sc_ctrl|U8_REGS_FLAG|1|67|33|SELF, SAVED|  "DO short circuit control",       DO_SC_EN[0...3], DO_SC_FLAG[4...7]; |
|21|do_ctrl|U16_REGS_FLAG|1|68|34|SELF|  "control digital output",      writeable with mask, DO_CTRL[0...3], DO_MASK[4...7]; |
|22|do_pwm_freq|U16_REGS_FLAG|1|70|35|SELF, SAVED|  "PWM frequency Hz",            DO_PWM_frequency[0...15]; |
|23|do_pwm_ctrl|U16_REGS_FLAG|4|72|36|SELF, SAVED|  "PWM control",                 DO_PWM_Duty[0...7], DO_PWM_Run[8] = {0-stop,1-run}; |
|24|di_noise_fltr_us|U16_REGS_FLAG|16|80|40|SELF, SAVED|  "digital inputs noise filter in us",  |
|25|di_pulseless_ms|U32_REGS_FLAG|16|112|56|SELF, SAVED|  "digital inputs pulseless time in ms",  |
|26|di_mode|U16_REGS_FLAG|16|176|88|SELF, SAVED|  "digital inputs mode",   see di_mode_t  |
|27|di_state|U32_REGS_FLAG|1|208|104|SELF, READ_ONLY, SAVED|  "digital inputs state",   |
|28|di_cnt|U64_REGS_FLAG|16|212|106|SELF, SAVED|  "digital inputs cnt values",  |
|29|di_freq|FLOAT_REGS_FLAG|16|340|170|SELF, SAVED|  "digital inputs frequency values",ro,  |
|30|ai_unit|U16_REGS_FLAG|8|404|202|SELF, READ_ONLY|  "14 bit capacity or 12 bit capacity for analog inputs",   |
|31|ai_state|U16_REGS_FLAG|1|420|210|SELF, READ_ONLY|   "bit range for adc channel, 1- if current > 4ma",  |
|32|ai_internal|U16_REGS_FLAG|8|422|211|SELF, READ_ONLY|  "12 bit capacity internal analog inputs", |
|33|ai_external|U16_REGS_FLAG|8|438|219|SELF, READ_ONLY|  "14 bit capacity external analog inputs", |
|34|internal_temp|FLOAT_REGS_FLAG|1|454|227|SELF, READ_ONLY|  "temperature internal sense value",  |
|35|external_temp|FLOAT_REGS_FLAG|1|458|229|SELF, READ_ONLY|  "temperature ADC sense value",  |
|36|v_pwr|FLOAT_REGS_FLAG|1|462|231|SELF, READ_ONLY|   "PWR voltage",  |
|37|v_bat|FLOAT_REGS_FLAG|1|466|233|SELF, READ_ONLY|   "3V battery voltage",  |
|38|sys_tick_counter|U64_REGS_FLAG|1|470|235|SELF, READ_ONLY|   "tick in ms",  |
|39|tick100us|U64_REGS_FLAG|1|478|239|SELF, READ_ONLY|  "tick counter in 100us time", |
|40|time_hms|U8_REGS_FLAG|10|486|243|SELF|  "struct for real time" |
|41|unix_time_sec|S32_REGS_FLAG|1|496|248|SELF|  "since the Epoch (00:00:00 UTC, January 1, 1970)"  |
|42|os_version|U8_REGS_FLAG|4|500|250|SELF, READ_ONLY|  "version by 0.1.1", |
|43|mac_addr|U8_REGS_FLAG|6|504|252|SELF, READ_ONLY|  "ethernet mac address", |
|44|flash_err_cnt|U32_REGS_FLAG|1|510|255|SELF, READ_ONLY|  "flash_err cnt", |
|45|flags_task|U32_REGS_FLAG|1|514|257|SELF, READ_ONLY|  "check for task created"  |
|46|counter_task|U64_REGS_FLAG|4|518|259|SELF, READ_ONLY|  "struct counter tasks"  |
|47|flags_init_passed|U32_REGS_FLAG|1|550|275|SELF, READ_ONLY|  "inited modules"  for init in task |
|48|flags_succ_init|U32_REGS_FLAG|1|554|277|SELF, READ_ONLY|  "success inited modules"  for init in task |
|49|isol_pwr_state|U16_REGS_FLAG|1|558|279|SELF, READ_ONLY|  "isolated power state",  |
|50|internal_task|U32_REGS_FLAG|1|560|280|SELF|   "internal_flash_dinamic_task" |
|51|external_task|U32_REGS_FLAG|1|564|282|SELF|   "external_flash_dinamic_task" |
|52|di_test_result|U32_REGS_FLAG|1|568|284|SELF, READ_ONLY|   "di_test result",       DI[0...15] = {1-OK, 0-error}, StateError[16], CntError[17], FreqError[18]; |
|53|do_test_result|U16_REGS_FLAG|1|572|286|SELF, READ_ONLY|   "do test result",       DO[0...3] = {1-OK, 0-error}, SCError[4], StateError[5], DutyError[6]; |
|54|ai_test_result|U16_REGS_FLAG|1|574|287|SELF, READ_ONLY|   "ai test result",       AI[0...7] = {1-OK, 0-error}, MeasureError[8], NoiseError[9], InstableError[10]; |
|55|sofi_test_result|U32_REGS_FLAG|1|576|288|SELF, READ_ONLY|   "sofi_test blocks results", Results[0...31] = {see sofi_test_block_t, 1-OK, 0-failed}; |
|56|sofi_test_blocks|U32_REGS_FLAG|1|580|290|SELF|   "sofi test blocks"     Blocks[0...28] = {see sofi_test_block_t},CheckTest[29],StressTest[30],PerfTest[31]; |
|57|run_test|U16_REGS_FLAG|1|584|292|SELF, READ_ONLY|  "running tests", |
|58|cur_free_heap|U32_REGS_FLAG|1|586|293|SELF, READ_ONLY|   in bytes, |
|59|min_free_heap|U32_REGS_FLAG|1|590|295|SELF, READ_ONLY|   in bytes, |
|60|debug_info|U8_REGS_FLAG|8|594|297|SELF|   "reserved use for debug" |
|61|rs_485_1_sends|U32_REGS_FLAG|1|602|301|SELF|   "RS-485_1 send num" |
|62|rs_485_1_errors|U32_REGS_FLAG|1|606|303|SELF|   "RS-485_1 errors" |
|63|rs_485_2_sends|U32_REGS_FLAG|1|610|305|SELF|   "RS-485_2 send num" |
|64|rs_485_2_errors|U32_REGS_FLAG|1|614|307|SELF|   "RS-485_2 errors" |
|65|rs_232_sends|U32_REGS_FLAG|1|618|309|SELF|   "RS-232 send num" |
|66|rs_232_errors|U32_REGS_FLAG|1|622|311|SELF|   "RS-232 errors" |
|67|command|U16_REGS_FLAG|1|626|313|SELF|   "Command register"         See @ref sofi_command_t |
|68|num_of_vars|U16_REGS_FLAG|1|628|314|SELF|   "number of vars self + config(user)   |
|69|current_os|U16_REGS_FLAG|1|630|315|SELF, READ_ONLY|   "using os 1 or 2"    |
|70|module_number|U16_REGS_FLAG|1|632|316|SELF, SAVED|  "module ao number 0 - 127",    |
|71|can_test|U32_REGS_FLAG|1|634|317|SELF|   accumulate errors packet |
|72|monitor_period|U32_REGS_FLAG|1|638|319|SELF, READ_ONLY|   "sofi_monitor period in ms", |
|73|total_tasks_time|FLOAT_REGS_FLAG|1|642|321|SELF, READ_ONLY|   "sum of running times of tasks in %", |
|74|task|U8_REGS_FLAG|28|646|323|SELF, READ_ONLY|  tasks information, |
|75|task|U8_REGS_FLAG|28|674|337|SELF, READ_ONLY|  tasks information, |
|76|task|U8_REGS_FLAG|28|702|351|SELF, READ_ONLY|  tasks information, |
|77|task|U8_REGS_FLAG|28|730|365|SELF, READ_ONLY|  tasks information, |
|78|task|U8_REGS_FLAG|28|758|379|SELF, READ_ONLY|  tasks information, |
|79|task|U8_REGS_FLAG|28|786|393|SELF, READ_ONLY|  tasks information, |
|80|task|U8_REGS_FLAG|28|814|407|SELF, READ_ONLY|  tasks information, |
|81|task|U8_REGS_FLAG|28|842|421|SELF, READ_ONLY|  tasks information, |
|82|task|U8_REGS_FLAG|28|870|435|SELF, READ_ONLY|  tasks information, |
|83|task|U8_REGS_FLAG|28|898|449|SELF, READ_ONLY|  tasks information, |
|84|task|U8_REGS_FLAG|28|926|463|SELF, READ_ONLY|  tasks information, |
|85|task|U8_REGS_FLAG|28|954|477|SELF, READ_ONLY|  tasks information, |
|86|task|U8_REGS_FLAG|28|982|491|SELF, READ_ONLY|  tasks information, |
|87|task|U8_REGS_FLAG|28|1010|505|SELF, READ_ONLY|  tasks information, |
|88|task|U8_REGS_FLAG|28|1038|519|SELF, READ_ONLY|  tasks information, |
|89|task|U8_REGS_FLAG|28|1066|533|SELF, READ_ONLY|  tasks information, |
|90|task|U8_REGS_FLAG|28|1094|547|SELF, READ_ONLY|  tasks information, |
|91|task|U8_REGS_FLAG|28|1122|561|SELF, READ_ONLY|  tasks information, |
|92|task|U8_REGS_FLAG|28|1150|575|SELF, READ_ONLY|  tasks information, |
|93|task|U8_REGS_FLAG|28|1178|589|SELF, READ_ONLY|  tasks information, |
|94|task|U8_REGS_FLAG|28|1206|603|SELF, READ_ONLY|  tasks information, |
|95|task|U8_REGS_FLAG|28|1234|617|SELF, READ_ONLY|  tasks information, |
|96|task|U8_REGS_FLAG|28|1262|631|SELF, READ_ONLY|  tasks information, |
|97|task|U8_REGS_FLAG|28|1290|645|SELF, READ_ONLY|  tasks information, |
|98|task|U8_REGS_FLAG|28|1318|659|SELF, READ_ONLY|  tasks information, |
|99|task|U8_REGS_FLAG|28|1346|673|SELF, READ_ONLY|  tasks information, |
|100|task|U8_REGS_FLAG|28|1374|687|SELF, READ_ONLY|  tasks information, |
|101|task|U8_REGS_FLAG|28|1402|701|SELF, READ_ONLY|  tasks information, |
|102|task|U8_REGS_FLAG|28|1430|715|SELF, READ_ONLY|  tasks information, |
|103|task|U8_REGS_FLAG|28|1458|729|SELF, READ_ONLY|  tasks information, |
|104|task|U8_REGS_FLAG|28|1486|743|SELF, READ_ONLY|  tasks information, |
|105|task|U8_REGS_FLAG|28|1514|757|SELF, READ_ONLY|  tasks information, |
|106|link|U16_REGS_FLAG|1|1542|771|SELF, READ_ONLY|    |
|107|eth_arp|U16_REGS_FLAG|1|1544|772|SELF, READ_ONLY|    |
|108|ip_frag|U16_REGS_FLAG|1|1546|773|SELF, READ_ONLY|    |
|109|ip_proto|U16_REGS_FLAG|1|1548|774|SELF, READ_ONLY|    |
|110|icmp|U16_REGS_FLAG|1|1550|775|SELF, READ_ONLY|    |
|111|udp|U16_REGS_FLAG|1|1552|776|SELF, READ_ONLY|    |
|112|tcp|U16_REGS_FLAG|1|1554|777|SELF, READ_ONLY|    |
|113|mem_heap|U16_REGS_FLAG|1|1556|778|SELF, READ_ONLY|   used heap memory by lwip, |
|114|memp_udp_pool|U16_REGS_FLAG|1|1558|779|SELF, READ_ONLY|    |
|115|memp_tcp_pool|U16_REGS_FLAG|1|1560|780|SELF, READ_ONLY|    |
|116|memp_listen_tcp|U16_REGS_FLAG|1|1562|781|SELF, READ_ONLY|    |
|117|memp_seg_tcp|U16_REGS_FLAG|1|1564|782|SELF, READ_ONLY|    |
|118|memp_altcp|U16_REGS_FLAG|1|1566|783|SELF, READ_ONLY|    |
|119|memp_reassdata|U16_REGS_FLAG|1|1568|784|SELF, READ_ONLY|    |
|120|memp_frag_pbuf|U16_REGS_FLAG|1|1570|785|SELF, READ_ONLY|    |
|121|memp_net_buf|U16_REGS_FLAG|1|1572|786|SELF, READ_ONLY|    |
|122|memp_net_conn|U16_REGS_FLAG|1|1574|787|SELF, READ_ONLY|    |
|123|memp_tcpip_api|U16_REGS_FLAG|1|1576|788|SELF, READ_ONLY|    |
|124|memp_tcpip_input|U16_REGS_FLAG|1|1578|789|SELF, READ_ONLY|    |
|125|memp_sys_timeout|U16_REGS_FLAG|1|1580|790|SELF, READ_ONLY|    |
|126|memp_pbuf_ref|U16_REGS_FLAG|1|1582|791|SELF, READ_ONLY|    |
|127|memp_pbuf_pool|U16_REGS_FLAG|1|1584|792|SELF, READ_ONLY|    |
|128|lwip_sys|U16_REGS_FLAG|1|1586|793|SELF, READ_ONLY|    |
