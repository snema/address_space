import sys
import argparse
import socket
import time
import mdb_tcp_udp_rtu
import json
import random
import base_test
import logging
import os
import struct
from inspect import currentframe, getframeinfo

__description__ = 'ping test'


class Test(base_test.Base):
    name = 'mdb_tcp_test'
    send_counter = 0
    receive_counter = 0
    min_time = 0
    max_time = 0
    avrg_time = 0
    frac_loss = 1.0
    ip_address = '192.168.1.232'
    tcp_port = 502
    udp_port = 7
    udp_port_self = 8
    max_buffer_size = 1512
    mdb_addr = 3
    mdb_funct = 3
    start_addr = 40008
    word_num = 2

    def __init__(self, ip_address='192.168.1.232', port="COM3",mdb_addr = 3,start_addr = 0,word_num = 2):
        self.send_counter = 0
        self.receive_counter = 0
        self.ip_address = ip_address
        self.mdb_addr = mdb_addr
        self.start_addr = start_addr
        self.word_num = word_num


    def set_ip_address(self, ip_address):
        self.ip_address = ip_address

    def test(self, check, stress, performance, infinity ):
        result = True
        if check:
            result = self.check() & result
        if stress:
            result = self.stress() & result
        if performance:
            result = self.performance() & result
        if infinity:
            result = self.infinity() & result
        return result

    def check(self, packet_num=10, max_packet_timeout=2000, packet_size=64):
        result = True

        """simply request mdb address"""
        mdb_base = [0x00, 0x03, 0x00, 0x00, 0x00, 0x04]
        mdb_tcp = mdb_tcp_udp_rtu.make_mdb_packet(mdb_base, self.mdb_addr, self.mdb_funct, self.start_addr, self.word_num)
        tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_socket.connect((self.ip_address, 502))
        try:
            lenght = len(mdb_tcp)-6
            mdb_tcp[4] = (lenght>>8)&0xff
            mdb_tcp[5] = lenght&0xff
            tcp_socket.settimeout(5)
            time_start = time.time()
            tcp_socket.send(bytearray(mdb_tcp[0:]))
            data = tcp_socket.recv(self.max_buffer_size)
            time_passed = time.time() - time_start
            data_s = []
            for i in range(0, len(data)):
                data_s.append(data[i])
            if mdb_tcp_udp_rtu.parse_mdb_tcp_response(data_s)>=0:
                if data_s[0]!=mdb_tcp[0] or data_s[1]!=mdb_tcp[1]:
                    result = False
            else:
                result = False
            self.print_log(data_s)

            self.print_log("length {}".format(len(data_s)))

            mdb_rtu = bytearray(mdb_tcp[6:])
            crc = mdb_tcp_udp_rtu.crc16(mdb_rtu, len(mdb_rtu))
            mdb_rtu.append(crc & 0xFF)
            mdb_rtu.append((crc >> 8) & 0xFF)
            tcp_socket.send(mdb_rtu)
            time_start = time.time()
            tcp_socket.settimeout(5)
            data = tcp_socket.recv(self.max_buffer_size)
            time_pr = time.time() - time_start
            data_s = []
            print(data)
            for i in range(0, len(data)):
                data_s.append(data[i])
            if mdb_tcp_udp_rtu.parse_mdb_response(data_s)<0 or\
               data_s[0] != self.mdb_addr or data_s[1] != self.mdb_funct:
                result = False
            self.print_log(data_s)
            self.print_log("length{}".format(len(data_s)))
        except OSError:
            result = False
            time.sleep(1)
            self.print_error("Can't send tcp Packet")
            try:
                tcp_socket.close()
                tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                tcp_socket.connect((self.ip_address, 502))
            except TimeoutError:
                time.sleep(1)
                self.print_error("mega12 not TCP connected ")
            except ConnectionAbortedError:
                time.sleep(1)
                self.print_error("mega12 connect aborted TCP")
                tcp_socket.connect((self.ip_address, 502))
            except OSError:
                time.sleep(1)
                self.print_error("mega12 connect aborted TCP")
        return result

    def stress(self, itterate=100):
        result = True
        return result

    def performance(self, itterate=100):
        result = True
        return result

    def infinity(self):
        logger = logging.getLogger('broadcasting')
        file_log_name = "broadcasting.log"
        if not os.path.exists(file_log_name):
            file_temp = open(file_log_name, 'w', encoding="utf-8")
            file_temp.close()
        log_path = os.path.join(os.getcwd(), file_log_name)
        file_handler = logging.FileHandler(file_log_name)
        formatter = logging.Formatter('%(asctime)s | %(levelname)-10s | %(message)s')
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        logger.setLevel(logging.DEBUG)
        logger_csv = logging.getLogger('broadcasting_csv')
        file_log_name = "broadcasting.csv"
        if not os.path.exists(file_log_name):
            file_temp = open(file_log_name, 'w', encoding="utf-8")
            file_temp.close()
        log_path = os.path.join(os.getcwd(), file_log_name)
        file_handler = logging.FileHandler(file_log_name)
        formatter = logging.Formatter('%(asctime)s ; %(levelname)-10s ; %(message)s')
        file_handler.setFormatter(formatter)
        logger_csv.addHandler(file_handler)
        logger_csv.setLevel(logging.DEBUG)
        result = True
        mdb_base = [0x00, 0x03, 0x00, 0x00, 0x00, 0x04]
        self.mdb_addr = 3
        self.start_addr = 40008
        self.word_num = 2
        mdb_tcp = mdb_tcp_udp_rtu.make_mdb_packet(mdb_base, self.mdb_addr, 3, self.start_addr, self.word_num)
        mdb_tcp_temp = mdb_tcp_udp_rtu.make_mdb_packet(mdb_base, self.mdb_addr, 3, 227, 8)
        tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_socket.connect((self.ip_address, 502))
        max_time = 0
        tr_id = 3
        id_mismatch = 0
        temperature = 0
        temperature_adc = 0
        while 1:
            try:
                #tcp over tcp
                tr_id+=1
                mdb_tcp[0] = (tr_id>>8)&0xff
                mdb_tcp[1] = (tr_id)&0xff
                lenght = len(mdb_tcp)-6
                mdb_tcp[4] = (lenght>>8)&0xff
                mdb_tcp[5] = lenght&0xff
                tcp_socket.settimeout(5)
                print(mdb_tcp[0:])
                time_start = time.time()
                tcp_socket.send(bytearray(mdb_tcp[0:]))
                data = tcp_socket.recv(self.max_buffer_size)
                time_passed = time.time() - time_start
                if time_passed > max_time:
                     max_time = time_passed
                data_receive = []
                for i in range(0, len(data)):
                    data_receive.append(data[i])
                if mdb_tcp_udp_rtu.parse_mdb_tcp_response(data_receive)>=0:
                    temp_value = data_receive[9]<<8
                    temp_value |= data_receive[10]
                    temp_value |= data_receive[11]<<24
                    temp_value |= data_receive[12]<<16
                    unix_time = temp_value
                    logger.info("id_mismatch : {}, unix_time read by can : {}, time_answer : {}, temp_int : {}, temp_adc : {}".format(id_mismatch,unix_time,time_passed,temperature,temperature_adc))
                    logger_csv.info("{};{};{};{};{}".format(id_mismatch,unix_time,time_passed,temperature,temperature_adc))
                    if data_receive[0]!=mdb_tcp[0] or data_receive[1]!=mdb_tcp[1]:
                        id_mismatch +=1
                print(data_receive)
                print("length", len(data_receive))
                print("time answear - {} s, max time answear {} s, id mismatch num {}".format(time_passed,max_time,id_mismatch))
                time.sleep(0.2)
                tr_id+=1
                mdb_tcp_temp[0] = (tr_id>>8)&0xff
                mdb_tcp_temp[1] = (tr_id)&0xff
                lenght = len(mdb_tcp_temp)-6
                mdb_tcp_temp[4] = (lenght>>8)&0xff
                mdb_tcp_temp[5] = lenght&0xff
                tcp_socket.settimeout(5)
                print(mdb_tcp_temp[0:])
                time_start = time.time()
                tcp_socket.send(bytearray(mdb_tcp_temp[0:]))
                data = tcp_socket.recv(self.max_buffer_size)
                time_passed = time.time() - time_start
                if time_passed > max_time:
                     max_time = time_passed
                data_receive = []
                for i in range(0, len(data)):
                    data_receive.append(data[i])
                if mdb_tcp_udp_rtu.parse_mdb_tcp_response(data_receive)>=0:
                    temp_value = int(data_receive[9])<<8
                    temp_value |= int(data_receive[10])
                    temp_value |= int(data_receive[11]<<24)
                    temp_value |= int(data_receive[12]<<16)
                    temperature_buffer = struct.pack('I',temp_value)
                    temperature_f = struct.unpack('f',temperature_buffer)
                    temperature = temperature_f[0]
                    temp_value = data_receive[13]<<8
                    temp_value |= data_receive[14]
                    temp_value |= data_receive[15]<<24
                    temp_value |= data_receive[16]<<16
                    temperature_bufer_adc = struct.pack('I',temp_value)
                    temperature_adc_f = struct.unpack('f',temperature_bufer_adc)
                    temperature_adc = temperature_adc_f[0]
                    if data_receive[0]!=mdb_tcp_temp[0] or data_receive[1]!=mdb_tcp_temp[1]:
                        id_mismatch +=1
                print(data_receive)
                print("length", len(data_receive))
                print("time answear - {} s, max time answear {} s, id mismatch num {}".format(time_passed,max_time,id_mismatch))


            except OSError:
                time.sleep(1)
                print("Can't send tcp Packet")
                error_log = open('error_log_rv.txt', 'a')
                error_log.write("mega12 connect aborted TCP "+time.asctime()+'\n')
                error_log.close()
                try:
                    print("cloze")
                    tcp_socket.close()
                    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    print("connect")
                    tcp_socket.connect((self.ip_address, 502))
                except TimeoutError:
                    time.sleep(1)
                    print(time.asctime())
                    print("mega12 not TCP connected ")
                    error_log = open('error_log_rv.txt','a')
                    error_log.write("mega12 not TCP connected "+time.asctime()+'\n')
                    error_log.close()
                except ConnectionAbortedError:
                    time.sleep(1)
                    print(time.asctime())
                    print("mega12 connect aborted TCP")
                    error_log = open('error_log_rv.txt','a')
                    error_log.write("mega12 connect aborted TCP"+time.asctime()+'\n')
                    error_log.close()
                    tcp_socket.connect((self.ip_address, 502))
                except OSError:
                    time.sleep(1)
                    print(time.asctime())
                    print("mega12 connect aborted TCP")
                    error_log = open('error_log_rv.txt','a')
                    error_log.write("mega12 connect aborted TCP"+time.asctime()+'\n')
                    error_log.close()
            time.sleep(0.2)
            q = self.get_ch()
            if q:
                print(ord(q))
                if ord(q) == 113:  # q
                    tcp_socket.close()
                    sys.exit(1)

        return result


def main():
    result = True
    parser = argparse.ArgumentParser(description=__description__)

    parser.add_argument('-c', '--check', action='store_true', default=False,
                        dest='check',
                        help='start check test')

    parser.add_argument('-s', '--stress', action='store_true', default=0,
                        dest='stress',
                        help=('start stress test'
                              '(default: %(default)s)'))
    parser.add_argument('-p', '--performance', action='store_true', default=0,
                        dest='performance',
                        help=('start performance test'
                              '(default: %(default)s)'))
    parser.add_argument('-i', '--infinity', action='store_true', default=0,
                        dest='infinity',
                        help=('start infinity test'
                              '(default: %(default)s)'))

    parser.add_argument('-ip', '--ip_address', type=str, default="192.168.1.232",
                        help=('path to global location'
                              '(default: %(default)s)'))
    parser.add_argument('-port', '--port', type=str, default="COM3",
                        help=('path to global location'
                              '(default: %(default)s)'))
    args = parser.parse_args()

    test = Test(args.ip_address, args.port)
    if args.check:
        result = test.check() & result
    if args.stress:
        result = test.stress() & result
    if args.performance:
        result = test.perfromance() & result
    if args.infinity:
        result = test.infinity() & result

    print("result ", result)
    print("total time", test.tot_time)


if __name__ == "__main__":
    main()
