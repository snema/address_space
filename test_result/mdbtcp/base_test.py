try:
    import msvcrt
    PLATFORM = "win"
except ImportError:
    PLATFORM = "unix"
    import tty
    import termios
    from select import select

class Base:
    name = 'base'
    tot_time  = 0
    tot_time_check  = 0
    tot_time_stress  = 0
    tot_time_performance = 0
    error_num = 0
    def __init__(self,name_test):
        self.name = name_test
    def check(self):
        print("not added check test")
    def stress(self):
        print("not added stress test")
    def performance(self):
        print("not added performance test")
    def print_error(self, info):
        self.error()
        print('# error', self.error_num," in test ", self.name, ":", info, '\n')

    def print_log(self, info):
        if self.name != "parse":
            print('# log'," in test ", self.name, ":", info, '\n')

    def error(self):
        self.error_num += 1
    def get_ch(self):
        if PLATFORM == "win":
            if msvcrt.kbhit():
                ch = msvcrt.getch()
                return ch
            else:
                return ""
        elif PLATFORM == "unix":
            fd = sys.stdin.fileno()
            old_setting = termios.tcgetattr(fd)
            try:
                tty.setraw(sys.stdin.fileno())
                i, o, e = select([sys.stdin.fileno()], [], [], 5)
                if i:
                    ch = sys.stdin.read(1)
                else:
                    ch = ""
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old_setting)
            return ch
        else:
            return ""

    
