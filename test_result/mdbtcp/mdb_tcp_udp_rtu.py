#!/c/Python33/ python
import sys
import os
import _thread as thread
import threading
import socket
import atexit
import io
import serial
import time
import struct
import random
import msvcrt
import base_test
"""
FB_MODBUS_Buffer[0] = IN->MODBUS_Addr.Data.uint8;     
FB_MODBUS_Buffer[1] = IN->MODBUS_Func.Data.uint8;     
FB_MODBUS_Buffer[2] = IN->RegAddr.Data.uint16 >> 8;   
FB_MODBUS_Buffer[3] = IN->RegAddr.Data.uint16 & 0xFF; 
FB_MODBUS_Buffer[4] = IN->RegNum.Data.uint16 >> 8;    
FB_MODBUS_Buffer[5] = IN->RegNum.Data.uint16 & 0xFF;  
CRC = crc16(FB_MODBUS_Buffer, LengthPak-2);
FB_MODBUS_Buffer[LengthPak-2] = (char)CRC;
FB_MODBUS_Buffer[LengthPak-1] = (char)(CRC>>8);
"""
"""
add addres property befor start program
"""

try:
    from serial.tools.list_ports import comports
except ImportError:
    comports = None

ip_address = '192.168.1.232'
tcp_port = 502
udp_port = 7
udp_port_self = 7
buffer_size = 1512


def udp_list(sock):
    while 1:
        data, address = sock.recvfrom(1024)
        print(address)
        print(data, len(data))
        data_s = []
        for i in range(0, len(data)):
            data_s.append(data[i])
        print(data_s)


def big_to_little(packet):
    for i in range(len(packet)//2):
        temp = packet[2*i]
        packet[2*i] = packet[2*i+1]
        packet[2*i+1] = temp


def com_list(ser):
    count = 0

    while 1:
        hello = ser.read(1)
        if hello != '':
            if int.from_bytes(hello, byteorder='big') == 3:
                count = 0
            else:
                count = count + len(hello)
            print(int.from_bytes(hello, byteorder='big'), count)


def make_mdb_packet(mdb_base = [], mdb_address=3, mdb_command=3, start_address=0, reg_num=1,data = [0,0]):
    reg_num_bytes = reg_num << 1
    mdb_packet = list(mdb_base)
    mdb_packet.append(mdb_address)
    mdb_packet.append(mdb_command)
    mdb_packet.append((start_address >> 8) & 0xff)
    mdb_packet.append(start_address & 0xff)
    if mdb_command == 3 or mdb_command == 4 or mdb_command == 1 or mdb_command == 2 or mdb_command == 0:
        mdb_packet.append((reg_num >> 8) & 0xff)
        mdb_packet.append(reg_num & 0xff)
    elif mdb_command == 16 or mdb_command == 15:
        mdb_packet.append((reg_num >> 8) & 0xff)
        mdb_packet.append(reg_num & 0xff)
        mdb_packet.append(reg_num_bytes & 0xff)
        for i in range(reg_num_bytes):
            mdb_packet.append(data[i] & 0xff)
    elif mdb_command == 6 or mdb_command == 5:
        mdb_packet.append(data[0] & 0xff)
        mdb_packet.append(data[1] & 0xff)
    else:
        print('command not responde')
    return mdb_packet



def main():
    have_serial = 1
    try:
        port_name = input("Select COM port: ")  # tutu
        ser = serial.Serial("COM"+port_name)#,parity = 'O',rtscts = 1)
        ser.baudrate = 115200
        print(ser.name)          # check which port was really used
        sys.stderr.write('--- Miniterm on %s: %d,%s,%s,%s ---\n' % (
            ser.portstr,
            ser.baudrate,
            ser.bytesize,
            ser.parity,
            ser.stopbits,
        ))
    except serial.SerialException as e:
        have_serial = 0
        print("could not open port ", port_name, "\n")
    mdb_base = [0x00, 0x03, 0x00, 0x00, 0x00, 0x04]
# packet info start
    mdb_address = 3
    mdb_command = 4
    start_address = 0
    reg_num = 1
    data = [0,6,12,88]
#packet info end
    mdb_tcp = make_mdb_packet(mdb_base, mdb_address, mdb_command, start_address, reg_num, data)
    print(mdb_tcp)
    ip_address = '192.168.2.206'
    tcp_port = 502
    udp_port = 7
    udp_port_self = 8
    buffer_size = 1512
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if have_serial:
        thread.start_new_thread(com_list, (ser, ))
        print('com tread is start')
    good_transaction = 0
    bad_transaction = 0
    print('c - tcp connect\n'
          't - modbus tcp send over tcp connect(after connect)\n'
          'r - modbus rtu send ocer tcp conect send(after connect)\n'
          'm - modbus RTU send over uart(open auto)\n'
          'u - modbus RTU send over udp(open auto)\n')
    arc_parse = 0
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("", udp_port_self))
    thread.start_new_thread(udp_list, (sock,))
    tr_id = 0
    while 1:
        q = msvcrt.getch()
        print(q)
        if ord(q) == 113: #q
            s.close()
            sys.exit(1)
        elif ord(q) == 119: #w
            print(mdbwrite)
            ser.write(mdbwrite)
        elif ord(q) == 114: #r
            mdb_tcp_temp = bytearray(mdb_tcp[6:])
            crc = crc16(mdb_tcp_temp, len(mdb_tcp_temp))
            mdb_tcp_temp.append(crc & 0xFF)
            mdb_tcp_temp.append((crc >> 8) & 0xFF)
            print(mdb_tcp_temp[0:])
            s.send(mdb_tcp_temp)
            time_start = time.time()
            s.settimeout(5)
            data = s.recv(buffer_size)
            time_pr = time.time() - time_start
            data_s = []
            print(data)
            for i in range(0, len(data)):
                data_s.append(data[i])
            parse_mdb_response(data_s)
            print(data_s)
            print("length", len(data_s))
            print(time_pr, 's')

        elif ord(q) == 109: #m
            mdb_rtu = mdb_tcp[6:]
            crc = crc16(mdb_rtu, len(mdb_rtu))
            mdb_rtu.append(crc & 0xFF)
            mdb_rtu.append((crc >> 8) & 0xFF)
            print(mdb_rtu)

            if have_serial:
                ser.reset_input_buffer()
                ser.write(mdb_rtu)
        elif ord(q) == 117: #u
            mdb_rtu = mdb_tcp[6:]
            crc = crc16(mdb_rtu, len(mdb_rtu))
            mdb_rtu.append(crc & 0xFF)
            mdb_rtu.append((crc >> 8) & 0xFF)
            print(mdb_rtu)
            packet_str = bytearray(mdb_rtu[0:])
            sock.sendto(packet_str, (ip_address, udp_port))
        elif ord(q) == 99: #c
            s.connect((ip_address, tcp_port))
        elif ord(q) == 116: #t
            tr_id+=1
            mdb_tcp[0] = (tr_id>>8)&0xff
            mdb_tcp[1] = (tr_id)&0xff
            lenght = len(mdb_tcp)-6
            mdb_tcp[4] = (lenght>>8)&0xff
            mdb_tcp[5] = lenght&0xff
            mdb_tcp_temp = bytearray(mdb_tcp[0:])
            print(mdb_tcp[0:])
            s.send(mdb_tcp_temp)
            time_start = time.time()
            s.settimeout(5)
            data = s.recv(buffer_size)
            time_pr = time.time() - time_start
            data_s = []
            for i in range(0, len(data)):
                data_s.append(data[i])
            parse_mdb_tcp_response(data_s)
            print(data_s)
            print("length", len(data_s))
            print(time_pr, 's')
        elif ord(q) == 108:#l
            while 1:
                mdb_rtu = mdb_tcp[6:]
                crc = crc16(mdb_rtu, len(mdb_rtu))                                                                                    
                mdb_rtu.append(crc & 0xFF)
                mdb_rtu.append((crc >> 8) & 0xFF)
                print(mdb_rtu)
                packet_str = bytearray(mdb_rtu[0:])
                sock.sendto(packet_str, (ip_address, udp_port))
                time.sleep(0.2)
                if msvcrt.kbhit():
                    q = msvcrt.getch()
                    print(ord(q))
                    if ord(q) == 113:#q
                        s.close()
                        sys.exit(1)


def parse_mdb_tcp_response(packet,name = "parse"):
    if len(packet)>=6:
        len_inside = ((packet[4]<<8)|packet[5])&0xffff
        if len(packet) < len_inside + 6:
            print("len mismatch")
            return -1
        else:
            parse_mdb_response(packet[6:],name)
        return 0
    else:
        return -1


def parse_mdb_response(packet, name = "parse"):
    base = base_test.Base(name)
    base.print_log('address {} -0x{}'.format(packet[0],hex(packet[0])))
    base.print_log('command {} -0x{}'.format(packet[1],hex(packet[1])))
    if packet[1] == 3 or packet[1]== 4:
        base.print_log('response byte {}'.format(packet[2]))
        for i in range(packet[2]//2):
            data = (packet[3+i*2] << 8) & 0xff00
            data |= (packet[3+i*2+1] & 0x00ff)
            #print('data of regs', i, '= ', data, 'hex', hex(data))
    elif packet[1] == 16:
        address = (packet[2] << 8) & 0xff00
        address |= (packet[3] & 0x00ff)
        base.print_log('start address {}'.format(address))
        number_write_reg = (packet[4] << 8) & 0xff00
        number_write_reg |= (packet[5] & 0x00ff)
        base.print_log('number_write_reg', number_write_reg, 'hex', hex(number_write_reg))

    elif packet[1] == 6:
        address = (packet[2] << 8) & 0xff00
        address |= (packet[3] & 0x00ff)
        base.print_log('start address {}'.format(address))
        reg_value = (packet[4] << 8) & 0xff00
        reg_value |= (packet[5] & 0x00ff)
        base.print_log('reg_value {} - 0x{}'.format(reg_value, hex(reg_value)))
    else:
        return -1
    return 0

  
def check_error_packet(data):
    if len(data) == 9:
        print(data[-3:-1])
        if data[-2] == 132:
            return 1
    return 0


def rtm64_crc(pbuffer, length):
    """CRC16 for RTM64"""
    crc = 0x0000
    k = 0
    while k < length:
        crc = (crc ^ (((pbuffer[k]) << 8) & 0xFFFF))
        k += 1
        i = 8
        while i:
            i -= 1
            if crc & 0x8000:
                crc = (((crc << 1) & 0xFFFF) ^ 0x1021)
            else:
                crc = ((crc << 1) & 0xFFFF)
    return crc


def crc16(pck, length):
    """CRC16 for modbus"""
    crc = 0xFFFF
    i = 0
    while i < length:
        crc ^= pck[i]
        i += 1
        j = 0
        while j < 8:
            j += 1
            if (crc & 0x0001) == 1:
                crc = ((crc >> 1) & 0xFFFF) ^ 0xA001
            else:
                crc >>= 1
    return crc & 0xFFFF


def rtm64_check_sum(buffer, length):
    """ CheckSum RTM64"""
    sum = 0
    i = 0
    while i < length:
        sum = sum + buffer[i]
        i += 1
    return sum


def int_to_char(cmd_x):
    """char to string array confersion"""
    i = 0
    cmd_r = ['~']
    while i < len(cmd_x):
        cmd_r += chr(int(cmd_x[i]))
        i += 1
    return cmd_r[1:]


def char_to_int(cmd_x, length):
    i = 0
    cmd_r = [0 for x in range(length)]
    while i < length:
        cmd_r[i] = ord(str(cmd_x[i]))
        i += 1
    return cmd_r


def print_hex(cmd, length):
    hex = [0 for i in range(length)]
    i = 0
    while i < length:
        hex[i] = (hex(cmd[i]))
        i += 1
    print(hex)


if __name__ == "__main__":
    main()
