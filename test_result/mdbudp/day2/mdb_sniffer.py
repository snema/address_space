import serial
import sys
import socket
from threading import Thread
import threading
import msvcrt
import time
from pprint import pprint as pp
import curses
import argparse
import mdb_lib
import logging
import os
import struct
__description__ = 'modbus sniffer'
default_baud_rate = 115200
default_ip = '192.168.1.232'
have_serial = 0
mdb_address = 6
mdb_command = 4
start_address = 227
reg_num = 100
default_data = [0,6,12,88]
MAX_RECEIVE_BYTE = 256
TIMEOUT = 0.5
udp_port_remote = 7
udp_port_self = 12345

def serial_ports():
    """ Lists serial port names
        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')
    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

receive_buff = [0 for x in range(MAX_RECEIVE_BYTE)]
receive_buff_len = 0
good_packet_num = 0
bad_packet_num = 0
class ComList(Thread):
    #recv_pack_checked = 0
    def __init__(self, interface, event_rx, event_stop):
        self.interface = interface
        self.event_rx = event_rx
        self.event_stop = event_stop
        self.receive_buff = [0 for x in range(MAX_RECEIVE_BYTE)]
        self.receive_buff_len = 0
        self.good_packet_num = 0
        self.bad_packet_num = 0
        self.packet_err = 0
        self.packet_num = 0
        self.running = 1
        self.logger = logging.getLogger('error_packet')
        file_log_name = "error_packet.log"
        if not os.path.exists(file_log_name):
            file_temp = open(file_log_name, 'w', encoding="utf-8")
            file_temp.close()
        log_path = os.path.join(os.getcwd(), file_log_name)
        file_handler = logging.FileHandler(file_log_name)
        formatter = logging.Formatter('%(message)s')
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)
        self.logger.setLevel(logging.DEBUG)

        super(ComList, self).__init__()

    def run(self):
        self.receive_byte_num = 0
        self.receive_timer = time.time()
        while self.running:
            if self.event_stop.wait(0):
                break
            if (time.time() > (self.receive_timer+0.015)) & (self.receive_byte_num!=0):
                #print("received: ",receive_byte_num)
                self.receive_byte_num -= 1
                crc_in_packet = self.receive_buff[self.receive_byte_num - 2] + (self.receive_buff[self.receive_byte_num - 1] << 8)
                if (mdb_lib.crc16(self.receive_buff, self.receive_byte_num-2) == crc_in_packet):
                    self.receive_buff_len = self.receive_byte_num
                    self.event_rx.set()
                    #parse_mdb_response(receive_buff,' ')
                else:
                    self.receive_buff_len = 0
                    self.event_rx.set()
                    self.error_pack = 1
                    self.packet_err += 1
                self.receive_byte_num = 0
                self.packet_num += 1
            self.interface.timeout = 0.001
            self.receive_char = self.interface.read(1)
            if self.receive_char:
                #print(receive_char,"recieve pack checked: ",recv_pack_checked)
                self.receive_timer = time.time()
                if self.receive_byte_num >= MAX_RECEIVE_BYTE or self.receive_byte_num < 0:
                    self.receive_byte_num = 0
                    self.receive_buff_len = 0
                    self.event_rx.set()
                if self.receive_byte_num== 0 and ord(self.receive_char)==0:
                    self.receive_byte_num = 0
                else:
                    self.receive_buff[self.receive_byte_num] = ord(self.receive_char)
                    self.receive_byte_num += 1
    
    def stop(self):
        self.running = 0

def main():
    parser = argparse.ArgumentParser(description=__description__)
    parser.add_argument('-ip', '--ip_address', type=str, default="192.168.1.232",
                        help=('path to global location'
                              '(default: %(default)s)'))
    parser.add_argument('-port', '--port', type=str, default="COM3",
                        help=('path to global location'
                              '(default: %(default)s)'))
    parser.add_argument('-u', '--udp', type=bool, default=False,
                        help=('using udp throuth ip instead COM interface'
                              '(default: %(default)s)'))

    args = parser.parse_args()
    have_serial = 1
    try:
        if args.udp:
            interface = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #        sock_recv = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            interface.bind(("", udp_port_self))
        else:
            interface = serial.Serial(args.port)#,parity = 'O',rtscts = 1)
            interface.baudrate = default_baud_rate
            print(interface.name)          # check which port was really used
            sys.stderr.write('--- Miniterm on %s: %d,%s,%s,%s ---\n' % (
                interface.portstr,
                interface.baudrate,
                interface.bytesize,
                interface.parity,
                interface.stopbits,
            ))
    except:
        have_serial = 0
        print("could not connect")
        exit()
    if have_serial:
        event_rx = threading.Event()
        event_stop = threading.Event()

        if args.udp==False:
            com_thread_id = ComList(interface,event_rx,event_stop)
            com_thread_id.start()
            print('treads is start')
        else:
            com_thread_id = 0
        curses.wrapper(broadcasting, interface, com_thread_id, event_rx, event_stop)

def broadcasting(screen, interface, com_thread_id, event_rx, event_stop):
    logger = logging.getLogger('broadcasting')
    file_log_name = "broadcasting.log"
    if not os.path.exists(file_log_name):
        file_temp = open(file_log_name, 'w', encoding="utf-8")
        file_temp.close()
    log_path = os.path.join(os.getcwd(), file_log_name)
    file_handler = logging.FileHandler(file_log_name)
    formatter = logging.Formatter('%(asctime)s | %(levelname)-10s | %(message)s')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.setLevel(logging.DEBUG)
    logger_csv = logging.getLogger('broadcasting_csv')
    file_log_name = "broadcasting.csv"
    if not os.path.exists(file_log_name):
        file_temp = open(file_log_name, 'w', encoding="utf-8")
        file_temp.close()
    log_path = os.path.join(os.getcwd(), file_log_name)
    file_handler = logging.FileHandler(file_log_name)
    formatter = logging.Formatter('%(asctime)s ; %(levelname)-10s ; %(message)s')
    file_handler.setFormatter(formatter)
    logger_csv.addHandler(file_handler)
    logger_csv.setLevel(logging.DEBUG)
    module_number = 0
    sdo_error = 0
    temperature = 0
    temperature_adc = 0
    time_start = time.time()
    time_send_packet = 0
    error_packets = 0
    error_packets_timeout = 0
    good_packets = 0
    percent = 0
    curses.curs_set(0)  # Hide the cursor
    screen.nodelay(True)  # Don't block I/O calls
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_WHITE)
    position = [0,0]
    debug = [1,0]
#packet info end
    mdb_rtu = mdb_lib.make_mdb_packet(mdb_address, mdb_command, start_address, reg_num, default_data)
    crc = mdb_lib.crc16(mdb_rtu, len(mdb_rtu))
    mdb_rtu.append(crc & 0xFF)
    mdb_rtu.append((crc >> 8) & 0xFF)
    logger_time = 0
    while True:
        try:
            time_current = time.time() - time_start
            time_str = str(time_current)

            screen.addstr(30,0, "time : "+time_str[:5])
            info_pos = len("time : ") + 12
            screen.addstr(30,info_pos, "good : {}".format(good_packets))
            info_pos += len("good") + 10
            screen.addstr(30,info_pos, "error : [{}:{}] ".format(error_packets,error_packets_timeout))
            info_pos += len("error") + 15
            screen.addstr(30,info_pos, "percent : {} ".format(str(percent)[:6]))
            info_pos += len("percent :") + 10
            screen.addstr(30,info_pos, "temp_in : {} ".format(str(temperature)[:6]))
            info_pos += len("temp_in :") + 10
            screen.addstr(30,info_pos, "temp_out : {} ".format(str(temperature_adc)[:6]))
            info_pos += len("temp_out :") + 10
            screen.addstr(30,info_pos, "sdo_error : {} ".format(sdo_error))
            screen.addstr(31,0, "module: {} ".format(module_number))

            if logger_time < time_current:
                logger_time = time_current + 20
                logger.info("baud : {}, time : {}, good : {}, error : [{}:{}], percent {} , temp_in {}, temp_adc {},sdo_error {}".format(default_baud_rate, str(time_current)[:6], good_packets,error_packets,error_packets_timeout,str(percent)[:6],str(temperature)[:5],str(temperature_adc)[:5],sdo_error))
                logger_csv.info("{};{};{};{};{};{};{};{}".format(default_baud_rate,str(time_current)[:6], good_packets,error_packets,str(percent)[:6],str(temperature)[:5],str(temperature_adc)[:5],sdo_error))
            if position[1] >= 120:
                position[1] = 0
                screen.erase()
            screen.addstr(*position, '-')
            position[1] += 1
            # Change direction on arrow keystroke
            key_input = screen.getch()
            if key_input == 113:#q
                event_stop.set()
                break
            elif key_input == 109:#m
                for i in range(len(mdb_rtu)):
                    temp_position = [i,position[1]]
                    screen.addstr(*temp_position, str(mdb_rtu[i]),curses.color_pair(2))
                position[1] += 3
                interface.reset_input_buffer()
                interface.write(mdb_rtu)
            if type(com_thread_id) == int:
                #udp transmitiong
                time_send_packet = time.time()
                for i in range(len(mdb_rtu)):
                    temp_position = [i,position[1]]
                    screen.addstr(*temp_position, str(mdb_rtu[i]),curses.color_pair(2))
                position[1] += 3
                try:
                    interface.settimeout(2)
                    packet_bytearray = bytearray(mdb_rtu)
                    interface.sendto(packet_bytearray, (default_ip, udp_port_remote))
                    time.sleep(0.01)
                    data_byte, address = interface.recvfrom(1024)
                    time_passed = time.time() - time_send_packet
                    length = len(data_byte)
                    print(length)
                    data_receive = [int(data_byte[i]) for i in range(len(data_byte))]
                    print("receive")

                    crc_in_packet = data_receive[length- 2] + (data_receive[length - 1] << 8)
                    if (mdb_lib.crc16(data_receive, length-2) == crc_in_packet):
                        good_packets += 1
                        percent = 100*((error_packets)/(good_packets+error_packets))
                        temp_value = int(data_receive[3])<<8
                        temp_value |= int(data_receive[4])
                        temp_value |= int(data_receive[5]<<24)
                        temp_value |= int(data_receive[6]<<16)
                        temperature_buffer = struct.pack('I',temp_value)
                        temperature_f = struct.unpack('f',temperature_buffer)
                        temperature = temperature_f[0]
                        temp_value = data_receive[7]<<8
                        temp_value |= data_receive[8]
                        temp_value |= data_receive[9]<<24
                        temp_value |= data_receive[10]<<16
                        temperature_bufer_adc = struct.pack('I',temp_value)
                        temperature_adc_f = struct.unpack('f',temperature_bufer_adc)
                        temperature_adc = temperature_adc_f[0]
                        temp_value = data_receive[181]<<8
                        temp_value |= data_receive[182]
                        module_number = temp_value
                        temp_value = data_receive[183]<<8
                        temp_value |= data_receive[184]
                        temp_value |= data_receive[185]<<24
                        temp_value |= data_receive[186]<<16
                        sdo_error = temp_value
                        length_max = length if length<25 else 25
                        for i in range(length_max):
                            temp_position = [i,position[1]]
                            screen.addstr(*temp_position, str(data_receive[i]),curses.color_pair(1))
                        temp_position = [length_max,position[1]]
                        screen.addstr(*temp_position, str(length),curses.color_pair(2))
                        position[1] += 3
                        time_send_packet = 0
                    else:
                        error_packets += 1
                except OSError:
                    error_packets += 1

            else:
                if event_rx.wait(0):
                    event_rx.clear()
                    time_send_packet = 0
                    if com_thread_id.receive_buff_len==0:
                        error_packets += 1
                    else:
                        good_packets += 1
                        percent = 100*((error_packets)/(good_packets+error_packets))
                        lentgh = com_thread_id.receive_buff_len if com_thread_id.receive_buff_len<25 else 25
                        temp_value = int(com_thread_id.receive_buff[3])<<8
                        temp_value |= int(com_thread_id.receive_buff[4])
                        temp_value |= int(com_thread_id.receive_buff[5]<<24)
                        temp_value |= int(com_thread_id.receive_buff[6]<<16)
                        temperature_buffer = struct.pack('I',temp_value)
                        temperature_f = struct.unpack('f',temperature_buffer)
                        temperature = temperature_f[0]
                        temp_value = com_thread_id.receive_buff[7]<<8
                        temp_value |= com_thread_id.receive_buff[8]
                        temp_value |= com_thread_id.receive_buff[9]<<24
                        temp_value |= com_thread_id.receive_buff[10]<<16
                        temperature_bufer_adc = struct.pack('I',temp_value)
                        temperature_adc_f = struct.unpack('f',temperature_bufer_adc)
                        temperature_adc = temperature_adc_f[0]

                        for i in range(lentgh):
                            temp_position = [i,position[1]]
                            screen.addstr(*temp_position, str(com_thread_id.receive_buff[i]),curses.color_pair(1))
                        temp_position = [lentgh,position[1]]
                        screen.addstr(*temp_position, str(com_thread_id.receive_buff_len),curses.color_pair(2))
                        position[1] += 3
                        time_send_packet = 0
                    time.sleep(0.1)

                if time_send_packet != 0:
                    time_passed = time.time() - time_send_packet
                    if time_passed>TIMEOUT:
                        time_send_packet = 0
                        error_packets += 1
                        error_packets_timeout += 1
                if time_send_packet == 0:
                    time_send_packet = time.time()
                    for i in range(len(mdb_rtu)):
                        temp_position = [i,position[1]]
                        screen.addstr(*temp_position, str(mdb_rtu[i]),curses.color_pair(2))
                    position[1] += 3
                    interface.reset_input_buffer()
                    interface.write(mdb_rtu)
            screen.refresh()
            time.sleep(0.1)
        except:
            if type(com_thread_id) != int:
                com_thread_id.stop()
            exit()

if __name__ == "__main__":
    main()