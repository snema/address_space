import logging
def big_to_little(packet):
    for i in range(len(packet)//2):
        temp = packet[2*i]
        packet[2*i] = packet[2*i+1]
        packet[2*i+1] = temp

def make_mdb_packet(mdb_address=3, mdb_command=3, start_address=0, reg_num=1,data = [0,0]):
    reg_num_bytes = reg_num << 1
    mdb_packet = []
    mdb_packet.append(mdb_address)
    mdb_packet.append(mdb_command)
    mdb_packet.append((start_address >> 8) & 0xff)
    mdb_packet.append(start_address & 0xff)
    if mdb_command == 3 or mdb_command == 4 or mdb_command == 1 or mdb_command == 2 or mdb_command == 0:
        mdb_packet.append((reg_num >> 8) & 0xff)
        mdb_packet.append(reg_num & 0xff)
    elif mdb_command == 16 or mdb_command == 15:
        mdb_packet.append((reg_num >> 8) & 0xff)
        mdb_packet.append(reg_num & 0xff)
        mdb_packet.append(reg_num_bytes & 0xff)
        for i in range(reg_num_bytes):
            mdb_packet.append(data[i] & 0xff)
    elif mdb_command == 6 or mdb_command == 5:
        mdb_packet.append(data[0] & 0xff)
        mdb_packet.append(data[1] & 0xff)
    else:
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        logger.info('command not responde')
    return mdb_packet

def parse_mdb_tcp_response(packet,name = "parse"):
    if len(packet)>=6:
        len_inside = ((packet[4]<<8)|packet[5])&0xffff
        if len(packet) < len_inside + 6:
            logger = logging.getLogger(__name__)
            logger.setLevel(logging.INFO)
            logger.info("len mismatch")
            return -1
        else:
            parse_mdb_response(packet[6:],name)
        return 0
    else:
        return -1


def parse_mdb_response(packet, name = "parse"):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    logger.info('address {} -0x{}'.format(packet[0],hex(packet[0])))
    logger.info('command {} -0x{}'.format(packet[1],hex(packet[1])))
    if packet[1] == 3 or packet[1]== 4:
        logger.info('response byte {}'.format(packet[2]))
        for i in range(packet[2]//2):
            data = (packet[3+i*2] << 8) & 0xff00
            data |= (packet[3+i*2+1] & 0x00ff)

    elif packet[1] == 16:
        address = (packet[2] << 8) & 0xff00
        address |= (packet[3] & 0x00ff)
        logger.info('start address {}'.format(address))
        number_write_reg = (packet[4] << 8) & 0xff00
        number_write_reg |= (packet[5] & 0x00ff)
        logger.info('number_write_reg', number_write_reg, 'hex', hex(number_write_reg))

    elif packet[1] == 6:
        address = (packet[2] << 8) & 0xff00
        address |= (packet[3] & 0x00ff)
        logger.info('start address {}'.format(address))
        reg_value = (packet[4] << 8) & 0xff00
        reg_value |= (packet[5] & 0x00ff)
        logger.info('reg_value {} - 0x{}'.format(reg_value, hex(reg_value)))
    else:
        return -1
    return 0

  
def check_error_packet(data):
    if len(data) == 9:
        if data[-2] == 132:
            return 1
    return 0


def rtm64_crc(pbuffer, length):
    """CRC16 for RTM64"""
    crc = 0x0000
    k = 0
    while k < length:
        crc = (crc ^ (((pbuffer[k]) << 8) & 0xFFFF))
        k += 1
        i = 8
        while i:
            i -= 1
            if crc & 0x8000:
                crc = (((crc << 1) & 0xFFFF) ^ 0x1021)
            else:
                crc = ((crc << 1) & 0xFFFF)
    return crc


def crc16(pck, length):
    """CRC16 for modbus"""
    crc = 0xFFFF
    i = 0
    while i < length:
        crc ^= pck[i]
        i += 1
        j = 0
        while j < 8:
            j += 1
            if (crc & 0x0001) == 1:
                crc = ((crc >> 1) & 0xFFFF) ^ 0xA001
            else:
                crc >>= 1
    return crc & 0xFFFF


def rtm64_check_sum(buffer, length):
    """ CheckSum RTM64"""
    sum = 0
    i = 0
    while i < length:
        sum = sum + buffer[i]
        i += 1
    return sum


def int_to_char(cmd_x):
    """char to string array confersion"""
    i = 0
    cmd_r = ['~']
    while i < len(cmd_x):
        cmd_r += chr(int(cmd_x[i]))
        i += 1
    return cmd_r[1:]


def char_to_int(cmd_x, length):
    i = 0
    cmd_r = [0 for x in range(length)]
    while i < length:
        cmd_r[i] = ord(str(cmd_x[i]))
        i += 1
    return cmd_r

