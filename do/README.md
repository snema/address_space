SOFI(DO) Address space
========================
|index|name|type|size|byte_address|mdb_address|flags|description|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|0|mdb_addr|U16_REGS_FLAG|1|0|0|SELF, SAVED|  "modbus address",  |
|1|device_type|U8_REGS_FLAG|1|2|1|SELF, READ_ONLY, SAVED|  "type of device",     |
|2|board_ver|U8_REGS_FLAG|1|3|1|SELF, READ_ONLY, SAVED|  "board version",     |
|3|module_number|U16_REGS_FLAG|1|4|2|SELF, SAVED|  "module do number 0 - 127",    |
|4|num_of_vars|U16_REGS_FLAG|1|6|3|SELF|   "number of vars self + config(user)   |
|5|ip|U8_REGS_FLAG|4|8|4|SELF, SAVED|  "ip address",  |
|6|netmask|U8_REGS_FLAG|4|12|6|SELF, SAVED|  "netmask address",  |
|7|gateaway|U8_REGS_FLAG|4|16|8|SELF, SAVED|  "gateaway address",  |
|8|usb_local_ip|U8_REGS_FLAG|4|20|10|SELF, SAVED|  "ip address for local usb net",   |
|9|mdb_revers|U8_REGS_FLAG|1|24|12|SELF, SAVED|  "reverse 3 and 4 function", |
|10|mdb_shift|U8_REGS_FLAG|1|25|12|SELF, SAVED|  "shift start address regs from 0 to 1", |
|11|reset_num|U16_REGS_FLAG|1|26|13|SELF, READ_ONLY, SAVED|  "number of system resets",  |
|12|last_reset|U16_REGS_FLAG|1|28|14|SELF, READ_ONLY, SAVED|  "reason of last system reset",     See rst_reason_t |
|13|sys_tick_counter|U64_REGS_FLAG|1|30|15|SELF, READ_ONLY|   "tick in ms",  |
|14|tick100us|U64_REGS_FLAG|1|38|19|SELF, READ_ONLY|  "tick counter in 100us time", |
|15|time_hms|U8_REGS_FLAG|10|46|23|SELF|  "struct for real time" |
|16|unix_time_sec|S32_REGS_FLAG|1|56|28|SELF|  "since the Epoch (00:00:00 UTC, January 1, 1970)"  |
|17|os_version|U8_REGS_FLAG|4|60|30|SELF, READ_ONLY|  "version by 0.1.1",  |
|18|mac_addr|U8_REGS_FLAG|6|64|32|SELF, READ_ONLY|  "mac address",  |
|19|uniq_id|U8_REGS_FLAG|12|70|35|SELF, READ_ONLY|  "uniq_id number" ,  |
|20|internal_temp|FLOAT_REGS_FLAG|1|82|41|SELF, READ_ONLY|  "temperature internal sense value",  |
|21|v_pwr|FLOAT_REGS_FLAG|1|86|43|SELF, READ_ONLY|   "PWR voltage"   |
|22|v_bat|FLOAT_REGS_FLAG|1|90|45|SELF, READ_ONLY|   "3V battery voltage"   |
|23|cur_free_heap|U32_REGS_FLAG|1|94|47|SELF, READ_ONLY|   in bytes, |
|24|min_free_heap|U32_REGS_FLAG|1|98|49|SELF, READ_ONLY|   in bytes, |
|25|do_test_result|U32_REGS_FLAG|1|102|51|SELF, READ_ONLY|   "do test result",       DO[0...7] = {1-OK, 0-error}, SCError[8], StateError[9], DutyError[10]; |
|26|sofi_test_result|U32_REGS_FLAG|1|106|53|SELF, READ_ONLY|  "sofi_test blocks results",  Results[0...31] = {see sofi_test_block_t, 1-OK, 0-failed}; |
|27|sofi_test_blocks|U32_REGS_FLAG|1|110|55|SELF|  "sofi test blocks"     Blocks[0...28] = {see sofi_test_block_t},CheckTest[29],StressTest[30],PerfTest[31]; |
|28|run_test|U32_REGS_FLAG|1|114|57|SELF, READ_ONLY|  "running tests",  |
|29|state|U32_REGS_FLAG|1|118|59|SELF, READ_ONLY|   "current module state"   |
|30|command|U16_REGS_FLAG|1|122|61|SELF|   "command register"   See @ref sofi_command_t  |
|31|debug_info|U8_REGS_FLAG|8|124|62|SELF|  "reserved use for debug" |
|32|uart1_sets|U16_REGS_FLAG|1|132|66|SELF, SAVED|  "settings immodule uart",  BitRateValue[0...3] = {0-default,1-2400,2-4800,3-9600,4-14400, |
|33|uart3_sets|U16_REGS_FLAG|1|134|67|SELF, SAVED|  "settings MESO_UART",  WordLen[4,5] = {0-7bit,1-8bit,2-9bit}; |
|34|channels_timeout|U32_REGS_FLAG|6|136|68|SELF, SAVED|   "time outs for channel use for retranslations", |
|35|do_state|U16_REGS_FLAG|1|160|80|SELF, READ_ONLY|  "state of digital output",  DO_ON[0...7], DO_SC[8...15]; |
|36|do_sc_ctrl|U16_REGS_FLAG|1|162|81|SELF, SAVED|  "DO short circuit control",       DO_SC_EN[0...7], DO_SC_FLAG[8...15]; |
|37|do_ctrl|U16_REGS_FLAG|1|164|82|SELF|  "control digital output",      writeable with mask, DO_CTRL[0...7], DO_MASK[8...15]; |
|38|do_pwm_freq|U16_REGS_FLAG|1|166|83|SELF, SAVED|  "PWM frequency Hz",            DO_PWM_frequency[0...15]; |
|39|do_pwm_ctrl|U16_REGS_FLAG|8|168|84|SELF, SAVED|  "PWM control",                 DO_PWM_Duty[0...7], DO_PWM_Run[8] = {0-stop,1-run}; |
|40|flags_task|U32_REGS_FLAG|1|184|92|SELF, READ_ONLY|  "check for task created"  |
|41|counter_task|U64_REGS_FLAG|4|188|94|SELF, READ_ONLY|  "struct counter tasks"  |
|42|flags_init_passed|U32_REGS_FLAG|1|220|110|SELF, READ_ONLY|  "inited modules"  for init in task |
|43|flags_succ_init|U32_REGS_FLAG|1|224|112|SELF, READ_ONLY|  "success inited modules"  for init in task |
|44|isol_pwr_state|U16_REGS_FLAG|1|228|114|SELF, READ_ONLY|  "isolated power state",  DO_PWR[0] = {1-OK, 0-error}, +5V_ISOL[1] = {1-OK, 0-error} |
|45|ai_internal|U16_REGS_FLAG|4|230|115|SELF, READ_ONLY|  "12 bit capacity internal analog inputs", |
|46|rs_485_immo_sends|U32_REGS_FLAG|1|238|119|SELF|   "RS-485_1 send num" |
|47|rs_485_immo_errors|U32_REGS_FLAG|1|242|121|SELF|   "RS-485_1 errors" |
|48|monitor_period|U32_REGS_FLAG|1|246|123|SELF, READ_ONLY|   "sofi_monitor period in ms", |
|49|total_tasks_time|FLOAT_REGS_FLAG|1|250|125|SELF, READ_ONLY|   "sum of running times of tasks in %", |
|50|task|U8_REGS_FLAG|28|254|127|SELF, READ_ONLY|  tasks information, |
|51|task|U8_REGS_FLAG|28|282|141|SELF, READ_ONLY|  tasks information, |
|52|task|U8_REGS_FLAG|28|310|155|SELF, READ_ONLY|  tasks information, |
|53|task|U8_REGS_FLAG|28|338|169|SELF, READ_ONLY|  tasks information, |
|54|task|U8_REGS_FLAG|28|366|183|SELF, READ_ONLY|  tasks information, |
|55|task|U8_REGS_FLAG|28|394|197|SELF, READ_ONLY|  tasks information, |
|56|task|U8_REGS_FLAG|28|422|211|SELF, READ_ONLY|  tasks information, |
|57|task|U8_REGS_FLAG|28|450|225|SELF, READ_ONLY|  tasks information, |
|58|task|U8_REGS_FLAG|28|478|239|SELF, READ_ONLY|  tasks information, |
|59|task|U8_REGS_FLAG|28|506|253|SELF, READ_ONLY|  tasks information, |
|60|task|U8_REGS_FLAG|28|534|267|SELF, READ_ONLY|  tasks information, |
|61|task|U8_REGS_FLAG|28|562|281|SELF, READ_ONLY|  tasks information, |
|62|task|U8_REGS_FLAG|28|590|295|SELF, READ_ONLY|  tasks information, |
|63|task|U8_REGS_FLAG|28|618|309|SELF, READ_ONLY|  tasks information, |
|64|task|U8_REGS_FLAG|28|646|323|SELF, READ_ONLY|  tasks information, |
|65|task|U8_REGS_FLAG|28|674|337|SELF, READ_ONLY|  tasks information, |
|66|task|U8_REGS_FLAG|28|702|351|SELF, READ_ONLY|  tasks information, |
|67|task|U8_REGS_FLAG|28|730|365|SELF, READ_ONLY|  tasks information, |
|68|task|U8_REGS_FLAG|28|758|379|SELF, READ_ONLY|  tasks information, |
|69|task|U8_REGS_FLAG|28|786|393|SELF, READ_ONLY|  tasks information, |
|70|task|U8_REGS_FLAG|28|814|407|SELF, READ_ONLY|  tasks information, |
|71|task|U8_REGS_FLAG|28|842|421|SELF, READ_ONLY|  tasks information, |
|72|task|U8_REGS_FLAG|28|870|435|SELF, READ_ONLY|  tasks information, |
|73|task|U8_REGS_FLAG|28|898|449|SELF, READ_ONLY|  tasks information, |
|74|task|U8_REGS_FLAG|28|926|463|SELF, READ_ONLY|  tasks information, |
|75|task|U8_REGS_FLAG|28|954|477|SELF, READ_ONLY|  tasks information, |
|76|task|U8_REGS_FLAG|28|982|491|SELF, READ_ONLY|  tasks information, |
|77|task|U8_REGS_FLAG|28|1010|505|SELF, READ_ONLY|  tasks information, |
|78|task|U8_REGS_FLAG|28|1038|519|SELF, READ_ONLY|  tasks information, |
|79|task|U8_REGS_FLAG|28|1066|533|SELF, READ_ONLY|  tasks information, |
|80|task|U8_REGS_FLAG|28|1094|547|SELF, READ_ONLY|  tasks information, |
|81|task|U8_REGS_FLAG|28|1122|561|SELF, READ_ONLY|  tasks information, |
|82|link|U16_REGS_FLAG|1|1150|575|SELF, READ_ONLY|    |
|83|eth_arp|U16_REGS_FLAG|1|1152|576|SELF, READ_ONLY|    |
|84|ip_frag|U16_REGS_FLAG|1|1154|577|SELF, READ_ONLY|    |
|85|ip_proto|U16_REGS_FLAG|1|1156|578|SELF, READ_ONLY|    |
|86|icmp|U16_REGS_FLAG|1|1158|579|SELF, READ_ONLY|    |
|87|udp|U16_REGS_FLAG|1|1160|580|SELF, READ_ONLY|    |
|88|tcp|U16_REGS_FLAG|1|1162|581|SELF, READ_ONLY|    |
|89|mem_heap|U16_REGS_FLAG|1|1164|582|SELF, READ_ONLY|   used heap memory by lwip, |
|90|memp_udp_pool|U16_REGS_FLAG|1|1166|583|SELF, READ_ONLY|    |
|91|memp_tcp_pool|U16_REGS_FLAG|1|1168|584|SELF, READ_ONLY|    |
|92|memp_listen_tcp|U16_REGS_FLAG|1|1170|585|SELF, READ_ONLY|    |
|93|memp_seg_tcp|U16_REGS_FLAG|1|1172|586|SELF, READ_ONLY|    |
|94|memp_altcp|U16_REGS_FLAG|1|1174|587|SELF, READ_ONLY|    |
|95|memp_reassdata|U16_REGS_FLAG|1|1176|588|SELF, READ_ONLY|    |
|96|memp_frag_pbuf|U16_REGS_FLAG|1|1178|589|SELF, READ_ONLY|    |
|97|memp_net_buf|U16_REGS_FLAG|1|1180|590|SELF, READ_ONLY|    |
|98|memp_net_conn|U16_REGS_FLAG|1|1182|591|SELF, READ_ONLY|    |
|99|memp_tcpip_api|U16_REGS_FLAG|1|1184|592|SELF, READ_ONLY|    |
|100|memp_tcpip_input|U16_REGS_FLAG|1|1186|593|SELF, READ_ONLY|    |
|101|memp_sys_timeout|U16_REGS_FLAG|1|1188|594|SELF, READ_ONLY|    |
|102|memp_pbuf_ref|U16_REGS_FLAG|1|1190|595|SELF, READ_ONLY|    |
|103|memp_pbuf_pool|U16_REGS_FLAG|1|1192|596|SELF, READ_ONLY|    |
|104|lwip_sys|U16_REGS_FLAG|1|1194|597|SELF, READ_ONLY|    |
